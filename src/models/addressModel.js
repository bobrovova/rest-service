const mongoose = require('mongoose');

/**
 * Address model
 */
const addressSchema = new mongoose.Schema({
    address: {
        type: String,
        unique: true,
    },
    balance: {
        type: String
    }
});

module.exports = mongoose.model('Address', addressSchema);