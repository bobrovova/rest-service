const dbConfig = require('./../config/dbConfig');
const mongoose = require('mongoose');
const log      = require('./logger');

mongoose.connect(dbConfig.url);

mongoose.connection.on('error', (err) => {
    log.error("Database Connection Error: " + err);
    process.exit(2);
});

mongoose.connection.on('connected', ()=>{
    log.info("Successfully connected to database");
});

require('./models');