const express       = require('express');
const mongoose      = require('mongoose').Mongoose;
const bodyParser    = require('body-parser');
const log           = require('./logger');
const app           = express();
const port = 7777;

require('./dbInit');

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: true
}));

app.use(require('./controllers'));

app.listen(port, () => {
    log.info('Server has been started');
});

module.exports = app;