const app       = (require('express').Router)();
const Address   = require('./../models/index').Address;
const log       = require('./../logger/index');

/**
 * [GET] /
 * @return All addresses
 */
app.get('/', (req, res) => {
    res.header('Access-Control-Allow-Origin', 'http://santiment.ru');
    res.header('Access-Control-Allow-Credentials', 'true');
    Address.find({}, (err, addressArr) => {
        if (err) {
            res.statusCode = 500;
            log.error('Internal error (%d): %s', res.statusCode, err.message);
            res.send({'error': 'Server error'});
        } else {
            res.send(addressArr);
        }
    });
});

/**
 * [POST] /
 * @param address
 * @param balance
 * @return Created address
 */
app.post('/', (req, res) => {
    const address = new Address({
        address: req.body.address,
        balance: req.body.balance
    });

    address.save((err) => {
        if(err){
            res.statusCode = 500;
            log.error('Internal error (%d): %s', res.statusCode, err.message);
            res.send({'error': 'Server error'});
        } else {
            res.send(address);
        }
    });
});

/**
 * [PUT] /:address
 * @param balance
 * @return Updated address
 */
app.put('/:address', (req, res) => {
    const address = req.params.address;
    const details = {
        address: address
    }
    const newData = {
        balance: req.body.balance
    };

    Address.updateOne(details, newData, (err, address) => {
        if(err){
            res.statusCode = 500;
            log.error('Internal error (%d): %s', res.statusCode, err.message);
            res.send({'error': 'Server error'});
        } else {
            res.send(address);
        }
    });
});

module.exports = app;
