const app = new (require('express').Router)();

app.use('/addresses/', require('./addressController'));

module.exports = app;